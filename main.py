from tkinter import *
#from founction import *
#from founction2 import *
import datetime
import random
import argparse
import time

def init(): #creation de la grille
    
    x =0  
    y =0  #positions

    for i in range(args.rows) : #lignes verticale
        if x!=canvash :
            myCanvas.create_line(x,0,x,canvash,width=1,fill='black')
            x+=args.size
    
    for j in range(canvasw): #lignes horizontales
        if y!=canvasw :
            myCanvas.create_line(0,y,canvasw,y,width=1,fill='black')
            y+=args.size

def processing_module():
    a=0
    b=0
    n=0
    for i in range(args.rows):  
        for j in range(args.column):
            if(args.percentage < random.random()) : 
                a=i*args.size
                b=j*args.size
                color = '#800000'
                case[i][j]= myCanvas.create_rectangle(a, b, a+args.size, b+args.size, fill=color)
                state[i][j] = 0
            else:
                a=i*args.size
                b=j*args.size
                color= '#013220'
                case[i][j]= myCanvas.create_rectangle(a, b, a+args.size, b+args.size, fill=color)
                state[i][j] = 1
                n+=1
    print("il y a " + str(n) + "arbres")

def left_click(event):
    
    height = 0
    horizontal_shift =0
    x = event.x -(event.x%args.size)  
    y = event.y -(event.y%args.size)
    for i in range(0, x, args.size) :
            height = height + 1
    for j in range(0, y, args.size) :
            horizontal_shift = horizontal_shift + 1
    if state[height][horizontal_shift] == 1 :  
        state[height][horizontal_shift] = 3
    else :
        state[height][horizontal_shift] = 5
        myCanvas.itemconfig(case[height][horizontal_shift], fill = '#efcc00')

def right_click(event):
    height=0
    horizontal_shift=0
    x = event.x -(event.x%args.size)
    y = event.y -(event.y%args.size)
    for i in range(0, x, args.size) :
            height = height + 1
    for j in range(0, y, args.size) :
            horizontal_shift = horizontal_shift + 1
    state[height][horizontal_shift] = 0
    myCanvas.itemconfig(case[height][horizontal_shift], fill = '#73654c')

    def fire() :
    
    for i in range(args.column):
        state[-1][i]=4
        state[0][i]=4
        flag = False
    for i in range(args.rows):
        state[i][-1]=4
        state[i][0]=4
        flag = False

    up=0
    nb=0
    t=0
    for i in range(args.column): #0= rien, 1= arbre, 3= feu, 4 = fin de propagation, 5= feu eteint
        for j in range(args.rows):
            if state[i][j]== 3 :
                nb = nb +1
                myCanvas.itemconfig(case[i][j],fill='red')
                
                if state[i+1][j] == 1:
                    up = up +1
                    state[i+1][j] = 3
                    myCanvas.itemconfig(case[i+1][j],fill='red')
                
                if state[i-1][j] == 1:
                    up = up +1
                    state[i-1][j] = 3
                    myCanvas.itemconfig(case[i-1][j],fill='red')
                
                if state[i][j+1] == 1:
                    up = up +1
                    state[i][j+1] = 3
                    myCanvas.itemconfig(case[i][j+1],fill='red')
                
                if state[i][j-1]== 1:
                    up = up +1
                    state[i][j-1]=3
                    myCanvas.itemconfig(case[i][j-1],fill='red')
                    
            elif state[i][j] == 5:
                state[i][j] = 6
                myCanvas.itemconfig(state[i][j], fill='red')
    
    for i in range(args.rows):
        for j in range(args.column):
            if state[i][j]== 3 :
                color= 'gray'
                myCanvas.itemconfig(case[i][j],fill=color)
            elif state[i][j] == 6 :
                color= 'gray'
                myCanvas.itemconfig(case[i][j],fill=color)
            
def temps():
    fire()

def passage():
    temps()
    root.after(speed,passage)

if __name__ == '__main__':

    parser=argparse.ArgumentParser(description="N")
    parser.add_argument("-rows", help="nombre de ligne(s) ", default=10, type=int)
    parser.add_argument("-column", help="nombre colonne(s)", default=10, type = int)
    parser.add_argument("-size", help="taille cellules", default=45, type = int)
    parser.add_argument("-percentage", help="pourcentage d'arbres", type = float, default=0.5)
    args = parser.parse_args()
    root = Tk()
    root.minsize(800,600)
    root.title("miniprojet-4")
    
    titre.pack(side=TOP)
    titre=Label(root, text = "Feu", font=("Noah", 14))
    
    global state, case
    state= [[0] * args.column for i in range(args.rows)]  
    case= [[0] * args.column for i in range(args.rows)] 
 
    global canvash, canvasw, proceed, to, speed 
    speed =1000
    to= False
    proceed=0
    canvasw= args.column*args.size    
    canvash = args.rows*args.size    #canvas widht and height
    myCanvas = Canvas(root, width=canvasw, height=canvash, bg='#d3d3d3', bd=0, highlightthickness=0)
    myCanvas.bind("<Button-1>", left_click)
    myCanvas.bind("<Button-3>", right_click)
    myCanvas.place(relx=0.5,rely=0.5,anchor=CENTER)
    myCanvas.pack(expand=YES)

    button = Frame(root)
    btn_quit = Button(button, text='Leave', command = root.destroy)
    btn_quit.grid(row=0, column=3, sticky='', padx=50)
    btn_re = Button(button, text='Reboot the forest', command = processing_module)
    btn_re.grid(row=0, column=0 , sticky='', padx=50)
    button.pack(side=BOTTOM, pady=10)

    #process
    init()
    processing_module()
    passage()
    root.mainloop()
