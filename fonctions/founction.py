def generate():
    
    a=0
    b=0
    n=0
    for i in range(args.rows):  
        for j in range(args.column):
            if(args.percentage < random.random()) : 
                a=i*args.size
                b=j*args.size
                color = '#800000'
                case[i][j]= myCanvas.create_rectangle(a, b, a+args.size, b+args.size, fill=color)
                etat_case[i][j] = 0
            else:
                a=i*args.size
                b=j*args.size
                color= '#013220'
                case[i][j]= myCanvas.create_rectangle(a, b, a+args.size, b+args.size, fill=color)
                etat_case[i][j] = 1
                n+=1
    print("il y a " + str(n) + "arbres")

                    
def init(): #creation de la grille
    
    x =0  
    y =0  #positions

    
    for i in range(args.rows) : #lignes verticale
        if x!=canvash :
            myCanvas.create_line(x,0,x,canvash,width=1,fill='black')
            x+=args.size
    
    for j in range(canvasw): #lignes horizontales
        if y!=canvasw :
            myCanvas.create_line(0,y,canvasw,y,width=1,fill='black')
            y+=args.size


def left_click(event):
    '''
        Cette fonction mettra en feu sur une cellule
    '''
    height = 0
    horizontal_shift =0
    x = event.x -(event.x%args.size)  
    y = event.y -(event.y%args.size)
    for i in range(0, x, args.size) :
            height+=1
    for j in range(0, y, args.size) :
            horizontal_shift+=1
    if etat_case[height][horizontal_shift] == 1 :  
        etat_case[height][horizontal_shift] = 3
    else :
        etat_case[height][horizontal_shift] = 5
        myCanvas.itemconfig(case[height][horizontal_shift], fill = '#efcc00')

def right_click(event):
    '''
        Cette fonction enlèvera le feu d'une cellule et la remplacer par la terre
    '''
    height=0
    horizontal_shift=0
    x = event.x -(event.x%args.size)
    y = event.y -(event.y%args.size)
    for i in range(0, x, args.size) :
            height+=1
    for j in range(0, y, args.size) :
            horizontal_shift+=1
    etat_case[height][horizontal_shift] = 0
    myCanvas.itemconfig(case[height][horizontal_shift], fill = '#73654c')
